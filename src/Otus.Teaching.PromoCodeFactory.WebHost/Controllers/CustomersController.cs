﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;

        public CustomersController(IRepository<Customer> customerRepository)
        {
            this._customerRepository = customerRepository;
        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersModelList = customers.Select(x =>
                new CustomerShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName
                }).ToList();

            return customersModelList;
        }

        /// <summary>
        /// Получить данные клиента по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var customerModel = new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                LastName = customer.LastName,
                FirstName = customer.FirstName,
                Preferences = customer.CustomerPreferences?.Select(x => new PreferenceResponse
                {
                    Id = x.Preference.Id,
                    Name = x.Preference.Name
                }).ToList(),
                PromoCodes = customer.PromoCodes?.Select(x => new PromoCodeShortResponse
                {
                    Id = x.Id,
                    PartnerName = x.PartnerName,
                    BeginDate = x.BeginDate.ToString(),
                    Code = x.Code,
                    EndDate = x.EndDate.ToString(),
                    ServiceInfo = x.ServiceInfo
                }).ToList()
            };

            return customerModel;
        }

        /// <summary>
        /// Создание нового клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            if (request is null)
                return BadRequest("Model is null");

            var entity = new Customer
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                CustomerPreferences = request.PreferenceIds.Select(x => new CustomerPreference
                {
                    PreferenceId = x
                }).ToList()
            };

            entity.Id = (await this._customerRepository.CreateAsync(entity)).Id;

            var result = new CustomerResponse
            {
                Id = entity.Id,
                Email = entity.Email,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
            };

            return Ok(result);
        }

        /// <summary>
        /// Редактирование клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ActionResult<CustomerResponse>> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            if (request is null)
                return BadRequest("Model is null");

            var existCustomer = await this._customerRepository.GetByIdAsync(id);
            if (existCustomer is null)
                return NotFound("Customer not found");

            existCustomer.FirstName = request.FirstName;
            existCustomer.LastName = request.LastName;
            existCustomer.Email = request.Email;

            await this._customerRepository.UpdateAsync(existCustomer);

            return Ok(existCustomer);
        }

        /// <summary>
        /// Удаление клиента
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult<bool>> DeleteCustomer(Guid id)
        {
            var existCustomer = this._customerRepository.GetByIdAsync(id);
            if (existCustomer is null)
                return NotFound("Customer not found");

            var result = await this._customerRepository.DeleteAsync(id);

            return Ok(result);
        }
    }
}