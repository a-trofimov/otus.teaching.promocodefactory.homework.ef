﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfigurations
{
    internal class EmployeeEntityTypeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.Property(b => b.FirstName).HasMaxLength(50);
            builder.Property(b => b.LastName).HasMaxLength(50);
            builder.Property(b => b.Email).HasMaxLength(62);

            builder.HasOne(e => e.Role)
                .WithMany(e => e.Employees)
                .HasForeignKey(e => e.RoleId);

            builder.HasData(FakeDataFactory.Employees);
        }
    }
}
