﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfigurations
{
    internal class CustomerPreferenceEntityTypeCinfiguration : IEntityTypeConfiguration<CustomerPreference>
    {
        public void Configure(EntityTypeBuilder<CustomerPreference> builder)
        {
            builder.HasOne(e => e.Customer)
                .WithMany(e => e.CustomerPreferences)
                .HasForeignKey(e => e.CustomerId);

            builder.HasOne(e => e.Preference)
                .WithMany(e => e.CustomerPreferences)
                .HasForeignKey(e => e.PreferenceId);

            builder.HasData(FakeDataFactory.CustomerPreferences);
        }
    }
}
