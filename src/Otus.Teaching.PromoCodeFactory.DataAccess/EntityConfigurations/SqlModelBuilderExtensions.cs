﻿using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfigurations
{
    public static class SqlModelBuilderExtensions
    {
        public static void ApplySqlEntityTypeConfigurations(this ModelBuilder modelBuilder)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string nameSpace = typeof(CustomerEntityTypeConfiguration).Namespace;
            modelBuilder.ApplyConfigurationsFromAssembly(assembly, type => type.Namespace == nameSpace);
        }
    }
}
