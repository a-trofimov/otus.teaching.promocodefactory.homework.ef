﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfigurations
{
    internal class PromoCodeEntityTypeConfiguration : IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder.Property(b => b.Code).HasMaxLength(50);
            builder.Property(b => b.ServiceInfo).HasMaxLength(150);
            builder.Property(b => b.PartnerName).HasMaxLength(100);

            builder.HasOne(e => e.Preference)
                .WithMany(e => e.PromoCodes)
                .HasForeignKey(e => e.PreferenceId);

            builder.HasOne(e => e.PartnerManager)
                .WithMany(e => e.PromoCodes)
                .HasForeignKey(e => e.PartnerManagerId);

            builder.HasOne(e => e.Customer)
                .WithMany(e => e.PromoCodes)
                .HasForeignKey(e => e.CustomerId);
        }
    }
}
