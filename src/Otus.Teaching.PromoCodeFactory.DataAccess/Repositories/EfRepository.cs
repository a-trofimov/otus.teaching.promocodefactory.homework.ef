﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        private readonly ApplicationDbContext _dbContext;

        public EfRepository(ApplicationDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public Task<List<T>> GetAllAsync()
        {
            return Task.FromResult(_dbContext.Set<T>().ToList());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return _dbContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public Task<T> CreateAsync(T entity)
        {
            if (entity is null)
                throw new ArgumentNullException("Model is null", nameof(entity));

            this._dbContext.Set<T>().Add(entity);
            this._dbContext.SaveChanges();

            return Task.FromResult(entity);
        }

        public Task<T> UpdateAsync(T entity)
        {
            if (entity is null)
                throw new ArgumentNullException("Model is null", nameof(entity));

            this._dbContext.Set<T>().Update(entity);
            this._dbContext.SaveChanges();

            return Task.FromResult(entity);
        }

        public Task<bool> DeleteAsync(Guid id)
        {
            var exist = this.GetByIdAsync(id);
            if (exist is null)
                throw new ArgumentNullException(nameof(exist));

            this._dbContext.Remove(exist);

            return Task.FromResult(true);
        }
    }
}
