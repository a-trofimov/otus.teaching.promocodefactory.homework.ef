﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EmployeeRepository : IRepository<Employee>
    {
        private readonly ApplicationDbContext _dbContext;

        public EmployeeRepository(ApplicationDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public Task<List<Employee>> GetAllAsync()
        {
            return Task.FromResult(_dbContext.Set<Employee>().ToList());
        }

        public Task<Employee> GetByIdAsync(Guid id)
        {
            return _dbContext.Set<Employee>().Include(e => e.Role).FirstOrDefaultAsync(x => x.Id == id);
        }

        public Task<Employee> CreateAsync(Employee entity)
        {
            if (entity is null)
                throw new ArgumentNullException("Model is null", nameof(entity));

            this._dbContext.Set<Employee>().Add(entity);
            this._dbContext.SaveChanges();

            return Task.FromResult(entity);
        }

        public Task<Employee> UpdateAsync(Employee entity)
        {
            if (entity is null)
                throw new ArgumentNullException("Model is null", nameof(entity));

            this._dbContext.Set<Employee>().Update(entity);
            this._dbContext.SaveChanges();

            return Task.FromResult(entity);
        }

        public Task<bool> DeleteAsync(Guid id)
        {
            var exist = this.GetByIdAsync(id);
            if (exist is null)
                throw new ArgumentNullException(nameof(exist));

            this._dbContext.Remove(exist);

            return Task.FromResult(true);
        }
    }
}
