﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PromocodeRepository : IRepository<PromoCode>
    {
        private readonly ApplicationDbContext _dbContext;

        public PromocodeRepository(ApplicationDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public Task<List<PromoCode>> GetAllAsync()
        {
            return Task.FromResult(_dbContext.Set<PromoCode>().ToList());
        }

        public Task<PromoCode> GetByIdAsync(Guid id)
        {
            return _dbContext.Set<PromoCode>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public Task<PromoCode> CreateAsync(PromoCode entity)
        {
            if (entity is null)
                throw new ArgumentNullException("Model is null", nameof(entity));

            // Предпочтение
            var preference = this._dbContext.Set<Preference>().FirstOrDefault(x => x.Id == entity.PreferenceId);
            if (preference is null)
                throw new ArgumentNullException($"{nameof(PromoCode.Preference)} is null");

            // Партнер
            var employee = this._dbContext.Set<Employee>().FirstOrDefault(x => x.Id == entity.PartnerManagerId);
            if (employee is null)
                throw new ArgumentNullException($"{nameof(PromoCode.PartnerManager)} is null");

            entity.PartnerManager = employee;
            entity.PartnerName = employee.FullName;
            entity.Preference = preference;

            var customer = this._dbContext.Set<Customer>()
                .Include(e => e.CustomerPreferences)
                .FirstOrDefault(x => x.CustomerPreferences.Any(x => x.PreferenceId == preference.Id));

            var dtNow = DateTime.Now;
            entity.CustomerId = customer.Id;
            entity.BeginDate = dtNow;
            entity.EndDate = dtNow.AddDays(7);

            this._dbContext.Set<PromoCode>().Add(entity);
            this._dbContext.SaveChanges();

            return Task.FromResult(entity);
        }

        public Task<PromoCode> UpdateAsync(PromoCode entity)
        {
            if (entity is null)
                throw new ArgumentNullException("Model is null", nameof(entity));

            this._dbContext.Set<PromoCode>().Update(entity);
            this._dbContext.SaveChanges();

            return Task.FromResult(entity);
        }

        public Task<bool> DeleteAsync(Guid id)
        {
            var exist = this.GetByIdAsync(id);
            if (exist is null)
                throw new ArgumentNullException(nameof(exist));

            this._dbContext.Remove(exist);

            return Task.FromResult(true);
        }
    }
}
