﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : IRepository<Customer>
    {
        private readonly ApplicationDbContext _dbContext;

        public CustomerRepository(ApplicationDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public Task<List<Customer>> GetAllAsync()
        {
            return Task.FromResult(_dbContext.Set<Customer>().ToList());
        }

        public Task<Customer> GetByIdAsync(Guid id)
        {
            return _dbContext.Set<Customer>()
                .Include(e => e.PromoCodes)
                .Include(e => e.CustomerPreferences)
                .ThenInclude(e => e.Preference)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public Task<Customer> CreateAsync(Customer entity)
        {
            if (entity is null)
                throw new ArgumentNullException("Model is null", nameof(entity));

            this._dbContext.Set<Customer>().Add(entity);

            entity.CustomerPreferences.ForEach(x => x.CustomerId = entity.Id);

            this._dbContext.Set<CustomerPreference>().AddRange(entity.CustomerPreferences);
            this._dbContext.SaveChanges();

            return Task.FromResult(entity);
        }

        public Task<Customer> UpdateAsync(Customer entity)
        {
            if (entity is null)
                throw new ArgumentNullException("Model is null", nameof(entity));

            this._dbContext.Set<Customer>().Update(entity);
            this._dbContext.SaveChanges();

            return Task.FromResult(entity);
        }

        public Task<bool> DeleteAsync(Guid id)
        {
            var exist = this.GetByIdAsync(id);
            if (exist is null)
                throw new ArgumentNullException(nameof(exist));

            this._dbContext.Remove(exist);

            return Task.FromResult(true);
        }
    }
}
